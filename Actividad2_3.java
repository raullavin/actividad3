/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculadora;
import Calculadora.Calculadora;
import java.util.Scanner;

/**
 *
 * @author alumno
 */
public class Actividad2_3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        Calculadora miCalc = new Calculadora();
        int num1;
        int num2;
        int resultado;
        
        try {
            System.out.println("Introduce el primer numero");
            num1 = entrada.nextInt();
            
            System.out.println("Introduce el segundo numero" );
            num2 = entrada.nextInt();
            
            if (num1 != 0 && num2 != 0) {
                resultado = miCalc.sumaEnteros(num1, num2);
                System.out.println("El resultado es : " + resultado);                
            }
            
        } catch (Exception e ) {
            System.out.println("Se ha producido un error");
        }
    }        
}
